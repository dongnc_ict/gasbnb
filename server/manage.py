import unittest
from app.config import Config
from flask_script import Manager
from app import create_app, blueprint
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_mail import Mail

app = create_app(Config.ENVI)
app.register_blueprint(blueprint)
app.app_context().push()
app.config['JWT_SECRET_KEY'] = Config.SECRET_KEY
jwt = JWTManager(app)
CORS(app)

app.config.update(Config.mail_settings)
mail = Mail(app)

manager = Manager(app)


@manager.command
def run():
    app.run()


@manager.command
def test():
    """Runs the unit tests."""
    tests = unittest.TestLoader().discover('tests', pattern='test*.py')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


if __name__ == '__main__':
    manager.run()
