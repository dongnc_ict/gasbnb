import os
import app.instance as instance_config
# Config class with built-in attribute for Flask and user-defined attribute for other usages
class Config:
    SECRET_KEY = instance_config.SECRET_KEY
    ENVI = instance_config.ENV
    DATABASE_CONFIG = instance_config.DATABASE_CONFIG
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))
    mail_settings = instance_config.mail_settings


class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    DEBUG = True
    TESTING = True
    PRESERVE_CONTEXT_ON_EXCEPTION = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class ProductionConfig(Config):
    DEBUG = False


configEnv = dict(
    dev=DevelopmentConfig,
    test=TestingConfig,
    prod=ProductionConfig
)
