from flask import request
from flask_restplus import Resource
from app.routes import api
from app.routes._api_models import accommodation_list_api_model, type_api_model
from app.controllers.type_controller import TypeController
from app.routes._query_params import pagination_params

type_ns = api.namespace('types', description='Operations for districts')


@type_ns.route('/')
class CityList(Resource):
    @type_ns.marshal_list_with(type_api_model)
    def get(self):
        return TypeController.get_all_types()


@type_ns.route('/<int:type_id>/accommodations')
class AccommodationOfCity(Resource):
    @type_ns.expect(pagination_params, validate=True)
    @type_ns.marshal_with(accommodation_list_api_model)
    def get(self, type_id):
        params = pagination_params.parse_args(request)
        limit = params.get('limit')
        offset = params.get('offset')
        return TypeController.get_accommodation_of_type(type_id, limit, offset)
