from flask_restplus import Api

api = Api(version='1.0', title='Gasbnb API',
          description='API for Gasbnb - Vietnam Vacation Rentals, Homes, Experiences & Places')


def error(code, message):
    return {
        "error": {
            "code": code,
            "message": message
        }
    }
