from flask import request
from flask_restplus import Resource
from app.routes import api
from app.controllers.accommodation_controller import AccommodationController
from app.routes._api_models import accommodation_api_model, accommodation_list_api_model
from app.routes._query_params import pagination_params, search_params

accommodation_ns = api.namespace('accommodations', description='Operations for accommodations')


@accommodation_ns.route('/')
class AccommodationList(Resource):
    @accommodation_ns.expect(search_params, validate=True)
    @accommodation_ns.marshal_with(accommodation_list_api_model)
    def get(self):
        params = search_params.parse_args(request)
        limit = params.get('limit')
        offset = params.get('offset')
        district = params.get('district')
        # checkin = params.get('checkin', '')
        # checkout = params.get('checkout', '')
        guests = params.get('guests')
        return AccommodationController.get_accommodation_list(limit, offset, district, guests)


@accommodation_ns.route('/<int:accommodation_id>')
class AccommodationList(Resource):
    @accommodation_ns.marshal_with(accommodation_api_model)
    def get(self, accommodation_id):
        return AccommodationController.get_accommodation(accommodation_id)

