from flask_restplus import Resource
from app.routes import api, error
from app.routes._query_params import booking_params
from app.models.booking import Booking
from app.controllers.booking_controller import BookingController
from app.controllers.user_controller import UserController
from app.controllers.accommodation_controller import AccommodationController
from flask_jwt_extended import (jwt_required, get_jwt_identity, get_raw_jwt)
from flask_mail import Message, Mail
from flask import current_app
from app.config import Config



booking_ns = api.namespace('book', description='Operations for booking')


@booking_ns.route('/')
class BookingAcco(Resource):
    @jwt_required
    @booking_ns.expect(booking_params)
    def post(self):
        data = booking_params.parse_args()
        booking = Booking()
        booking.accommodation_id = data['accommodationId']
        booking.checkin = data['checkin']
        booking.checkout = data['checkout']
        booking.guests = data['guestCount']
        booking.user_id = get_jwt_identity()['id']
        BookingController.insert(booking)
        acco = AccommodationController.get_accommodation(data['accommodationId'])
        guest = UserController.get_user_by_id(booking.user_id)
        with current_app.app_context():
            mail = Mail(current_app)
            msg1 = Message(subject="Your accommodation is booked",
                          sender=Config.mail_settings['MAIL_USERNAME'],
                          recipients=[acco.host.email],
                          body="Your accommodation is booked")
            mail.send(msg1)
            msg2 = Message(subject="Your reservation is recognized",
                           sender=Config.mail_settings['MAIL_USERNAME'],
                           recipients=[guest.email],
                           body="Your reservation is recognized")
            mail.send(msg2)



