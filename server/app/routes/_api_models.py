from app.routes import api
from flask_restplus import fields


district_api_model = api.model('District', {
    'id': fields.Integer(description='city id'),
    'name': fields.String(required=True, description='name of city'),
})

city_api_model = api.model('City', {
    'id': fields.Integer(description='city id'),
    'name': fields.String(required=True, description='name of city'),
    'districts': fields.List(fields.Nested(district_api_model))
})

type_api_model = api.model('Type', {
    'id': fields.Integer(description='type id'),
    'name': fields.String(required=True, description='name of accommodation type'),
})

amenity_api_model = api.model('Amenity', {
    'id': fields.Integer(description='amenity id'),
    'name': fields.String(required=True, description='name of amenity'),
})

_accommodation_smr_api_model = api.model('AccommodationSmr', {
    'id': fields.Integer(),
    'title': fields.String(),
    'type': fields.String(attribute='type.name'),
    'price': fields.Float(),
    'nBeds': fields.Integer(attribute='n_beds'),
    'pictures': fields.List(fields.String),
    'latitude': fields.Float(),
    'longitude': fields.Float()
})

accommodation_api_model = api.inherit('Accommodation', _accommodation_smr_api_model, {
    'cleanFee': fields.Float(attribute='clean_fee'),
    'serviceFee': fields.Float(attribute='service_fee'),
    'nBedrooms': fields.Integer(attribute='n_bedrooms'),
    'nBathrooms': fields.Integer(attribute='n_bathrooms'),
    'maxGuests': fields.Integer(attribute='max_guests'),
    'district': fields.String(attribute='district.name'),
    'city': fields.String(attribute='district.city.name'),
    'description': fields.String(),
    'policy': fields.String(),
    'amenities': fields.List(fields.Nested(amenity_api_model))
})

_pagination_api_model = api.model('Pagination', {
    'totalItem': fields.Integer(),
    'currentItemCount': fields.Integer(),
    'itemsPerPage': fields.Integer(),
    'totalPages': fields.Integer(),
    'currentPage': fields.Integer()
})

pagination_wrap_api_model = api.model('PaginationWrap', {
    'pagination': fields.Nested(_pagination_api_model)
})

accommodation_list_api_model = api.model('AccommodationList', {
    'pagination': fields.Nested(_pagination_api_model),
    'accommodations': fields.List(fields.Nested(_accommodation_smr_api_model))
})