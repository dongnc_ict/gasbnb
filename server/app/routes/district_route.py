from flask import request
from flask_restplus import Resource
from app.routes import api
from app.routes._api_models import accommodation_list_api_model, district_api_model
from app.controllers.district_controller import DistrictController
from app.routes._query_params import pagination_params

district_ns = api.namespace('districts', description='Operations for districts')


@district_ns.route('/')
class CityList(Resource):
    @district_ns.marshal_list_with(district_api_model)
    def get(self):
        return DistrictController.get_all_districts()


@district_ns.route('/<int:district_id>/accommodations')
class AccommodationOfCity(Resource):
    @district_ns.expect(pagination_params, validate=True)
    @district_ns.marshal_with(accommodation_list_api_model)
    def get(self, district_id):
        params = pagination_params.parse_args(request)
        limit = params.get('limit')
        offset = params.get('offset')
        return DistrictController.get_accommodation_of_district(district_id, limit, offset)
