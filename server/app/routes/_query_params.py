from flask_restplus import reqparse

pagination_params = reqparse.RequestParser()
pagination_params.add_argument('limit', type=int, required=False, default=10, help='Limit number of items')
pagination_params.add_argument('offset', type=int, required=False, default=0, help='Number of items to skip')

signup_params = reqparse.RequestParser()
signup_params.add_argument('email', type=str, required=True)
signup_params.add_argument('password', type=str, required=True)
signup_params.add_argument('firstName', type=str, required=True)
signup_params.add_argument('lastName', type=str, required=True)

login_params = reqparse.RequestParser()
login_params.add_argument('email', type=str, required=True)
login_params.add_argument('password', type=str, required=True)

fb_login_params = reqparse.RequestParser()
fb_login_params.add_argument('email', type=str, required=True)
fb_login_params.add_argument('name', type=str, required=True)
fb_login_params.add_argument('accessToken', type=str, required=True)

search_params = pagination_params.copy()
search_params.add_argument('district', type=int, required=False)
search_params.add_argument('guests', type=str, required=False)

booking_params = reqparse.RequestParser()
booking_params.add_argument('accommodationId', type=int, required=True)
booking_params.add_argument('checkin', type=int, required=True)
booking_params.add_argument('checkout', type=int, required=True)
booking_params.add_argument('guestCount', type=int, required=True)
