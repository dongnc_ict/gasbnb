from flask import request
from flask_restplus import Resource
from app.routes import api
from app.controllers.city_controller import CityController
from app.routes._api_models import city_api_model, accommodation_list_api_model, district_api_model
from app.routes._query_params import pagination_params

city_ns = api.namespace('cities', description='Operations for cities')


@city_ns.route('/')
class CityList(Resource):
    @city_ns.marshal_list_with(city_api_model)
    def get(self):
        return CityController.get_all_cities()


@city_ns.route('/<int:city_id>/accommodations')
class AccommodationOfCity(Resource):
    @city_ns.expect(pagination_params, validate=True)
    @city_ns.marshal_with(accommodation_list_api_model)
    def get(self, city_id):
        params = pagination_params.parse_args(request)
        limit = params.get('limit')
        offset = params.get('offset')
        return CityController.get_accommodation_of_city(city_id, limit, offset)


@city_ns.route('/<int:city_id>/districts')
class DistrictsOfCity(Resource):
    @city_ns.marshal_with(district_api_model)
    def get(self, city_id):
        return CityController.get_districts_of_city(city_id)
