from flask import request, abort
from flask_restplus import Resource
from app.routes import api, error
from app.routes._query_params import signup_params, login_params, fb_login_params
from app.models.user import User
from app.controllers.user_controller import UserController
import flask_bcrypt
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required,
                                get_jwt_identity, get_raw_jwt)
import requests
import string
import random


@api.route('/signup')
@api.expect(signup_params)
class Signup(Resource):
    def post(self):
        data = signup_params.parse_args()
        user = User()
        user.email = data['email']
        user.first_name = data['firstName']
        user.last_name = data['lastName']
        user.is_host = False
        if UserController.check_user_exists(user.email):
            return error('ERR_0010', 'User {} already exists'.format(data['email'])), 400

        user.password = flask_bcrypt.generate_password_hash(data['password']).decode('utf-8')
        UserController.insert(user)


@api.route('/login')
@api.expect(login_params)
class Login(Resource):
    def post(self):
        data = login_params.parse_args()
        user = UserController.get_user_by_email(data['email'])
        if not user:
            return error('ERR_0110', 'Email or password is incorrect'), 400

        if not flask_bcrypt.check_password_hash(user.password, data['password']):
            return error('ERR_0110', 'Email or password is incorrect'), 400
        else:
            identity = {
                "id": user.id,
                "email": user.email
            }
            access_token = create_access_token(identity=identity)
            refresh_token = create_refresh_token(identity=identity)
            return {
                'id': user.id,
                'email': user.email,
                'firstName': user.first_name,
                'lastName': user.last_name,
                'accessToken': access_token,
                'refreshToken': refresh_token
            }


@api.route('/login/fb')
@api.expect(fb_login_params)
class Test(Resource):
    def post(self):
        data = fb_login_params.parse_args()
        user_token = data['accessToken']
        link = 'https://graph.facebook.com/v3.2/me?fields=id%2Cemail&access_token=' + user_token
        try:
            response = requests.get(link).json()
            if response['email'] == data['email']:
                db_user = UserController.get_user_by_email(data['email'])
                if not db_user:  # if user not exist
                    # save to database
                    user = User()
                    user.email = data['email']
                    user.first_name = data['name']
                    user.last_name = ''
                    user.is_host = False
                    user.fb_id = response['id']
                    # random password
                    all_char = string.ascii_letters + string.punctuation + string.digits
                    password = "".join(random.choice(all_char) for x in range(random.randint(8, 20)))
                    user.password = flask_bcrypt.generate_password_hash(password).decode('utf-8')
                    UserController.insert(user)
                else:  # user exists
                    user = db_user
                    if not db_user.fb_id:  # already signup by this email but with normal way (not facebook way)
                        return error('ERR_0210', "User already signup by this email, please enter password"), 401
                identity = {
                    "id": user.id,
                    "email": user.email
                }
                access_token = create_access_token(identity=identity)
                refresh_token = create_refresh_token(identity=identity)
                return {
                    'id': user.id,
                    'email': user.email,
                    'firstName': user.first_name,
                    'lastName': user.last_name,
                    'accessToken': access_token,
                    'refreshToken': refresh_token
                }
            else:
                return error('ERR_0211', "User info and access token mismatch"), 400
        except (ValueError, KeyError, TypeError):
            return error('ERR_0212', "Invalid access token"), 400
