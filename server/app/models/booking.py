from app.models._meta import Base
from sqlalchemy import Column, Integer, ForeignKey
from sqlalchemy.orm import relationship


class Booking(Base):
    __tablename__ = 'booking'
    __table_args__ = {'autoload': True}
    user_id = Column(Integer, ForeignKey('user.id'))
    accommodation_id = Column(Integer, ForeignKey('accommodation.id'))
    accommodation = relationship("Accommodation", back_populates="bookings")

    def __repr__(self):
        return "<Booking - '%s': '%d' - '%d>" % (self.id, self.accommodation_id, self.user_id)

