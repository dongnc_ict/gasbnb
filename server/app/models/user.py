from app.models._meta import Base
import app.models.accommodation
from sqlalchemy.orm import relationship


class User(Base):
    __tablename__ = 'user'
    __table_args__ = {'autoload': True}
    accommodations = relationship("Accommodation", back_populates="host")

    def __repr__(self):
        return "<User - '%s': '%s'>" % (self.id, self.first_name)
