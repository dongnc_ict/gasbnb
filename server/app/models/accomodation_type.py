from app.models._meta import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, ForeignKey


class AccommodationType(Base):
    __tablename__ = 'accommodation_type'
    __table_args__ = {'autoload': True}
    accommodations = relationship("Accommodation", back_populates="type")
