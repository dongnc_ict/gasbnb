from app.models._meta import Base
from app.models.acco_amen import acco_amen
from sqlalchemy.orm import relationship

class Amenity(Base):
    __tablename__ = 'amenity'
    __table_args__ = {'autoload': True}
    accommodations = relationship("Accommodation", secondary=acco_amen, back_populates="amenities")

