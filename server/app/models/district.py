from app.models._meta import Base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, ForeignKey


class District(Base):
    __tablename__ = 'district'
    __table_args__ = {'autoload': True, 'extend_existing': True}
    city_id = Column(Integer, ForeignKey('city.id'))
    city = relationship("City", back_populates="districts")
    accommodations = relationship("Accommodation", back_populates="district", lazy='dynamic')

    def __init__(self, name=''):
        self.name = name
