from app.models._meta import Base
from sqlalchemy import Column, Integer, ForeignKey, Table

acco_amen = Table('acco_amen', Base.metadata,
                  Column('accommodation_id', Integer, ForeignKey('accommodation.id')),
                  Column('amenity_id', Integer, ForeignKey('amenity.id'))
                  )
