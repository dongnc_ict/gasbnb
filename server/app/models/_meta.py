from app.config import Config
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

db_config = Config.DATABASE_CONFIG
db_engine = create_engine('mysql://' +
                          db_config['user'] + ':' +
                          db_config['password'] +
                          '@localhost/' +
                          db_config['dbname'] + '?charset=utf8',
                          pool_size=20, max_overflow=50)

# create a configured "Session" class
Session = sessionmaker(bind=db_engine)
Base = declarative_base(bind=db_engine)
