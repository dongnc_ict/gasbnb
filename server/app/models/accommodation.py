from app.models._meta import Base
from app.models.acco_amen import acco_amen
from sqlalchemy import Column, Integer, ForeignKey, JSON
from sqlalchemy.orm import relationship
import json


class Accommodation(Base):
    __tablename__ = 'accommodation'
    __table_args__ = {'autoload': True}
    pictures = Column(JSON)
    host_id = Column(Integer, ForeignKey('user.id'))
    district_id = Column(Integer, ForeignKey('district.id'))
    type_id = Column(Integer, ForeignKey('accommodation_type.id'))
    host = relationship("User", back_populates="accommodations")
    district = relationship("District", back_populates="accommodations")
    type = relationship("AccommodationType", back_populates="accommodations")
    amenities = relationship("Amenity", secondary=acco_amen, back_populates="accommodations")
    bookings = relationship("Booking", back_populates="accommodation")

    def __repr__(self):
        return "<Accommodation - '%s': '%s'>" % (self.id, self.title)
