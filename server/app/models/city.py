from app.models._meta import Base
from sqlalchemy.orm import relationship


class City(Base):
    __tablename__ = 'city'
    __table_args__ = {'autoload': True}
    districts = relationship("District", back_populates="city")

    def __init__(self, name=''):
        self.name = name
