from flask import Flask, Blueprint
from app.routes import api
from app.routes.city_route import city_ns
from app.routes.district_route import district_ns
from app.routes.accommodation_route import accommodation_ns
from app.routes.type_route import type_ns
from app.routes.booking_route import booking_ns
import app.routes.authentication_route
from app.config import configEnv

blueprint = Blueprint('api', __name__)
api.init_app(blueprint)
api.add_namespace(city_ns, path='/cities')
api.add_namespace(district_ns, path='/districts')
api.add_namespace(accommodation_ns, path='/accommodations')
api.add_namespace(type_ns, path='/types')
api.add_namespace(booking_ns, path='/book')


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(configEnv[config_name])
    # db.init_app(app)
    # flask_bcrypt.init_app(app)
    return app
