from app.models.accommodation import Accommodation
from app.models.accomodation_type import AccommodationType
from app.models import Session
from sqlalchemy import func
import math


class TypeController:
    @staticmethod
    def get_all_types():
        session = Session()
        return session.query(AccommodationType).all()

    @staticmethod
    def get_accommodation_of_type(type_id, limit, offset):
        session = Session()
        accommodations = session.query(Accommodation).filter_by(type_id=type_id). \
            limit(limit).offset(offset)
        total_item = session.query(func.count(Accommodation.id)).filter_by(type_id=type_id).scalar()
        pagination = {
            'totalItem': total_item,
            'currentItemCount': accommodations.count(),
            'itemsPerPage': limit,
            'totalPages': int(math.ceil(total_item / limit)),
            'currentPage': round(offset / limit + 1)
        }

        return {
            'pagination': pagination,
            'accommodations': accommodations
        }
