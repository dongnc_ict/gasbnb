from app.models.accommodation import Accommodation
from app.models.district import District
from app.models import Session
from sqlalchemy import func
import math


class DistrictController:
    @staticmethod
    def get_all_districts():
        session = Session()
        return session.query(District).all()

    @staticmethod
    def get_accommodation_of_district(district_id, limit, offset):
        session = Session()
        accommodations = session.query(Accommodation).filter_by(district_id=district_id). \
            limit(limit).offset(offset)

        total_item = session.query(func.count(Accommodation.id)).filter_by(district_id=district_id).scalar()
        pagination = {
            'totalItem': total_item,
            'currentItemCount': accommodations.count(),
            'itemsPerPage': limit,
            'totalPages': int(math.ceil(total_item / limit)),
            'currentPage': round(offset / limit + 1)
        }

        return {
            'pagination': pagination,
            'accommodations': accommodations
        }
