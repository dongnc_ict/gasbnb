from app.models.city import City
from app.models.district import District
from app.models.accommodation import Accommodation
from app.models import Session
from sqlalchemy import func
import math


class CityController:
    @staticmethod
    def get_all_cities():
        session = Session()
        return session.query(City).all()

    @staticmethod
    def get_accommodation_of_city(city_id, limit, offset):
        session = Session()
        accommodations = session.query(Accommodation). \
            join(Accommodation.district). \
            join(District.city). \
            filter(City.id == city_id).limit(limit).offset(offset).all()
        total_item = session.query(func.count(Accommodation.id)). \
            join(Accommodation.district). \
            join(District.city). \
            filter(City.id == city_id).\
            scalar()
        pagination = {
            'totalItem': total_item,
            'currentItemCount': len(accommodations),
            'itemsPerPage': limit,
            'totalPages': int(math.ceil(total_item / limit)),
            'currentPage': round(offset / limit + 1)
        }
        return {
            'pagination': pagination,
            'accommodations': accommodations
        }

    @staticmethod
    def get_districts_of_city(city_id):
        session = Session()
        return session.query(District).filter(District.city_id == city_id).all()

