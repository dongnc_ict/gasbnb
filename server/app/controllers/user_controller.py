from app.models.city import City
from app.models.district import District
from app.models.accommodation import Accommodation
from app.models import Session
from app.models.user import User
from sqlalchemy import func
import math


class UserController:
    @staticmethod
    def insert(user):
        session = Session()
        session.add(user)
        session.commit()

    @staticmethod
    def get_user_by_email(email):
        session = Session()
        return session.query(User).filter(User.email == email).first()

    @staticmethod
    def check_user_exists(email):
        return bool(UserController.get_user_by_email(email))

    @staticmethod
    def get_user_by_id(user_id):
        session = Session()
        return session.query(User).filter(User.id == user_id).first()

