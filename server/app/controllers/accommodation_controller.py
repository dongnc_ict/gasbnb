from app.models.accommodation import Accommodation
from app.models import Session
from sqlalchemy import func
import math


class AccommodationController:
    @staticmethod
    def get_accommodation_list(limit, offset, district, guests):
        session = Session()
        query = session.query(Accommodation)
        count_query = session.query(func.count(Accommodation.id))
        if guests is not None:
            query = query.filter(Accommodation.max_guests >= guests)
            count_query = count_query.filter(Accommodation.max_guests >= guests)
        if district is not None:
            query = query.filter(Accommodation.district_id == district)
            count_query = count_query.filter(Accommodation.district_id == district)
        accommodations = query.limit(limit).offset(offset)
        total_item = count_query.scalar()
        pagination = {
            'totalItem': total_item,
            'currentItemCount': accommodations.count(),
            'itemsPerPage': limit,
            'totalPages': int(math.ceil(total_item / limit)),
            'currentPage': round(offset / limit + 1)
        }

        return {
            'pagination': pagination,
            'accommodations': accommodations
        }

    @staticmethod
    def get_accommodation(acco_id):
        session = Session()
        return session.query(Accommodation).filter(Accommodation.id == acco_id).first()

