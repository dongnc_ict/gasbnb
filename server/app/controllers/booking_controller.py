from app.models import Session


class BookingController:
    @staticmethod
    def insert(booking):
        session = Session()
        session.add(booking)
        session.commit()