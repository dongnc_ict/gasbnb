"""add data to city, district

Revision ID: abea630b9acb
Revises: 3a3e4bf6d544
Create Date: 2018-12-07 16:37:36.870896

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'abea630b9acb'
down_revision = '3a3e4bf6d544'
branch_labels = None
depends_on = None

city_table = sa.table('city',
                      sa.column('id', sa.Integer),
                      sa.column('name', sa.String)
                      )

district_table = sa.table('district',
                          sa.Column('id', sa.Integer),
                          sa.Column('name', sa.String),
                          sa.Column('city_id', sa.Integer)
                          )


def upgrade():
    op.bulk_insert(city_table,
                   [
                       {'id': 1, 'name': 'Hà Nội'},
                       {'id': 2, 'name': 'Hồ Chí Minh'}
                   ])
    op.bulk_insert(district_table,
                   [
                        {'name': 'Ba Đình', 'city_id': 1},
                        {'name': 'Hoàn Kiếm', 'city_id': 1},
                        {'name': 'Hai Bà Trưng', 'city_id': 1},
                        {'name': 'Đống Đa', 'city_id': 1},
                        {'name': 'Tây Hồ', 'city_id': 1},
                        {'name': 'Cầu Giấy', 'city_id': 1},
                        {'name': 'Thanh Xuân', 'city_id': 1},
                        {'name': 'Hoàng Mai', 'city_id': 1},
                        {'name': 'Long Biên', 'city_id': 1},
                        {'name': 'Bắc Từ Liêm', 'city_id': 1},
                        {'name': 'Thanh Trì', 'city_id': 1},
                        {'name': 'Gia Lâm', 'city_id': 1},
                        {'name': 'Đông Anh', 'city_id': 1},
                        {'name': 'Sóc Sơn', 'city_id': 1},
                        {'name': 'Hà Đông', 'city_id': 1},
                        {'name': 'Sơn Tây', 'city_id': 1},
                        {'name': 'Ba Vì', 'city_id': 1},
                        {'name': 'Phúc Thọ', 'city_id': 1},
                        {'name': 'Thạch Thất', 'city_id': 1},
                        {'name': 'Quốc Oai', 'city_id': 1},
                        {'name': 'Chương Mỹ', 'city_id': 1},
                        {'name': 'Đan Phượng', 'city_id': 1},
                        {'name': 'Hoài Đức', 'city_id': 1},
                        {'name': 'Thanh Oai', 'city_id': 1},
                        {'name': 'Mỹ Đức', 'city_id': 1},
                        {'name': 'Ứng Hòa', 'city_id': 1},
                        {'name': 'Thường Tín', 'city_id': 1},
                        {'name': 'Phú Xuyên', 'city_id': 1},
                        {'name': 'Mê Linh', 'city_id': 1},
                        {'name': 'Nam Từ Liêm', 'city_id': 1},
                        {'name': 'Quận 1', 'city_id': 2},
                        {'name': 'Quận 2', 'city_id': 2},
                        {'name': 'Quận 3', 'city_id': 2},
                        {'name': 'Quận 4', 'city_id': 2},
                        {'name': 'Quận 5', 'city_id': 2},
                        {'name': 'Quận 6', 'city_id': 2},
                        {'name': 'Quận 7', 'city_id': 2},
                        {'name': 'Quận 8', 'city_id': 2},
                        {'name': 'Quận 9', 'city_id': 2},
                        {'name': 'Quận 10', 'city_id': 2},
                        {'name': 'Quận 11', 'city_id': 2},
                        {'name': 'Quận 12', 'city_id': 2},
                        {'name': 'Gò Vấp', 'city_id': 2},
                        {'name': 'Tân Bình', 'city_id': 2},
                        {'name': 'Tân Phú', 'city_id': 2},
                        {'name': 'Bình Thạnh', 'city_id': 2},
                        {'name': 'Phú Nhuận', 'city_id': 2},
                        {'name': 'Thủ Đức', 'city_id': 2},
                        {'name': 'Bình Tân', 'city_id': 2},
                        {'name': 'Bình Chánh', 'city_id': 2},
                        {'name': 'Củ Chi', 'city_id': 2},
                        {'name': 'Hóc Môn', 'city_id': 2},
                        {'name': 'Nhà Bè', 'city_id': 2},
                        {'name': 'Cần Giờ', 'city_id': 2}
                   ])

def downgrade():
    op.execute('SET FOREIGN_KEY_CHECKS = 0')
    op.execute('TRUNCATE table city')
    op.execute('TRUNCATE table district')
    op.execute('SET FOREIGN_KEY_CHECKS = 1')
