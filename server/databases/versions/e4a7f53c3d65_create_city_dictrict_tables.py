"""create city, district tables

Revision ID: e4a7f53c3d65
Revises: c4d36749a74e
Create Date: 2018-12-02 19:07:44.826170

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'e4a7f53c3d65'
down_revision = 'c4d36749a74e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'city',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
    )
    op.create_table(
        'district',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
        sa.Column('city_id', sa.Integer, sa.ForeignKey("city.id")),
    )


def downgrade():
    op.drop_table('district')
    op.drop_table('city')
