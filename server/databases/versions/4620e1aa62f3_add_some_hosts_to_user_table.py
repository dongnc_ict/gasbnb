"""add some hosts to user table

Revision ID: 4620e1aa62f3
Revises: 6d2d6595b38b
Create Date: 2018-12-12 00:51:44.443193

"""
from alembic import op
import sqlalchemy as sa
import flask_bcrypt
import string
import random

# revision identifiers, used by Alembic.
revision = '4620e1aa62f3'
down_revision = '6d2d6595b38b'
branch_labels = None
depends_on = None

user_table = sa.table(
    'user',
    sa.Column('id', sa.Integer),
    sa.Column('is_host', sa.Boolean),
    sa.Column('email', sa.String),
    sa.Column('first_name', sa.String),
    sa.Column('last_name', sa.String),
    sa.Column('password', sa.String),
    sa.Column('fb_id', sa.String),
)


def upgrade():
    users = [
        {'is_host': 1, 'email': 'dongnc.ict@gmail.com', 'first_name': 'Chí Đông', 'last_name': 'Nguyễn'},
        {'is_host': 1, 'email': 'hoangson13896@gmail.com', 'first_name': 'Sơn', 'last_name': 'Hoàng'},
        {'is_host': 1, 'email': 'ngocpham0501@gmail.com', 'first_name': 'Minh Ngọc', 'last_name': 'Phạm'},
        {'is_host': 1, 'email': 'tunguyen.hust@gmail.com', 'first_name': 'Anh Tú', 'last_name': 'Nguyễn'}
    ]
    all_char = string.ascii_letters + string.punctuation + string.digits
    for user in users:
        # random password
        password = "".join(random.choice(all_char) for x in range(random.randint(8, 20)))
        user['password'] = flask_bcrypt.generate_password_hash(password).decode('utf-8')

    op.bulk_insert(user_table, users)


def downgrade():
    op.execute('SET FOREIGN_KEY_CHECKS = 0')
    op.execute('TRUNCATE table user')
    op.execute('SET FOREIGN_KEY_CHECKS = 1')