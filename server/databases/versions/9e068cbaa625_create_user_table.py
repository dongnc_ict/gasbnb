"""create user table

Revision ID: 9e068cbaa625
Revises: 
Create Date: 2018-12-02 06:39:36.790965

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '9e068cbaa625'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'user',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('is_host', sa.Boolean, nullable=False),
        sa.Column('email', sa.String(100), nullable=False, unique=True),
        sa.Column('first_name', sa.String(50), nullable=False),
        sa.Column('last_name', sa.String(50)),
        sa.Column('password', sa.String(256), nullable=False),
        sa.Column('fb_id', sa.String(20), unique=True),
    )


def downgrade():
    op.drop_table('user')
