"""add data to accommodation

Revision ID: 01873cf2c05b
Revises: b98153ae14dd
Create Date: 2018-12-12 23:11:01.850780

"""
from alembic import op
import sqlalchemy as sa
import csv
import os, sys, random, json
sys.path.append(os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__)))))
from app.models.district import District
from app.models.accommodation import Accommodation
from app.models.city import City
from app.models.acco_amen import acco_amen
from app.models.accomodation_type import AccommodationType
from app.models.amenity import Amenity
from app.models.user import User
from app.models import Session

# revision identifiers, used by Alembic.
revision = '01873cf2c05b'
down_revision = 'b98153ae14dd'
branch_labels = None
depends_on = None


def upgrade():
    session = Session()
    districts = session.query(District).all()
    types = session.query(AccommodationType).all()
    amenities = session.query(Amenity).all()
    hosts = session.query(User).filter(User.is_host == 1).all()
    host_ids = [user.id for user in hosts]

    csv_file_path = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "accommodation.csv")
    with open(csv_file_path, encoding='utf-8') as csvfile:
        csvreader = csv.reader(csvfile)
        line_count = -1
        for row in csvreader:
            line_count += 1
            if line_count != 0:
                accommodation = Accommodation()
                accommodation.title = row[1]
                accommodation.description = row[2]
                accommodation.policy = row[3]
                accommodation.latitude = float(row[6])
                accommodation.longitude = float(row[7])
                accommodation.max_guests = int(row[10])
                accommodation.price = float(row[11])
                accommodation.n_beds = int(row[14])
                accommodation.n_bedrooms = int(row[15])
                accommodation.n_bathrooms = int(row[16])
                accommodation.host_id = random.choice(host_ids)
                accommodation.clean_fee = round(random.uniform(0, 10), 2)
                accommodation.service_fee = round(random.uniform(0, 10), 2)

                # check district
                for district in districts:
                    if row[4].strip() == district.name:
                        accommodation.district_id = district.id
                        break
                else:
                    print("Invalid district [{}] of line {} -> discard".format(row[4], line_count))
                    continue

                # check type
                for acco_type in types:
                    if row[9].strip() == acco_type.name:
                        accommodation.type_id = acco_type.id
                        break
                else:
                    print("Invalid type [{}] of line {} -> discard".format(row[9], line_count))
                    continue

                # check pictures
                try:
                    pictures = json.loads(row[8])
                    accommodation.pictures = json.dumps(pictures)
                except ValueError as error:
                    print("Invalid json array of line {} -> discard".format(line_count))
                    continue

                session.add(accommodation)
                session.commit()

                acco_amenities = [item.strip() for item in row[17].split('\n')]
                for acco_item in acco_amenities:
                    for item in amenities:
                        if item.name == acco_item:
                            op.execute(
                                acco_amen.insert().values(
                                accommodation_id=accommodation.id,
                                amenity_id=item.id
                                )
                            )
                            break


def downgrade():
    op.execute('SET FOREIGN_KEY_CHECKS = 0')
    op.execute('TRUNCATE table accommodation')
    op.execute('TRUNCATE table acco_amen')
    op.execute('SET FOREIGN_KEY_CHECKS = 1')
