"""create acco_amen table

Revision ID: 3a3e4bf6d544
Revises: 8de18eeb63d3
Create Date: 2018-12-02 20:21:44.623226

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '3a3e4bf6d544'
down_revision = '8de18eeb63d3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'acco_amen',
        sa.Column('accommodation_id', sa.Integer, sa.ForeignKey('accommodation.id'), primary_key=True),
        sa.Column('amenity_id', sa.Integer, sa.ForeignKey('amenity.id'), primary_key=True),
    )


def downgrade():
    op.drop_table('acco_amen')
