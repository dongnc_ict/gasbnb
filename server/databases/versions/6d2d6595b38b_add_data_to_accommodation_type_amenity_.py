"""add data to accommodation_type, amenity tables

Revision ID: 6d2d6595b38b
Revises: abea630b9acb
Create Date: 2018-12-10 13:31:17.801127

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6d2d6595b38b'
down_revision = 'abea630b9acb'
branch_labels = None
depends_on = None


amenity_table = sa.table('amenity',
                      sa.column('id', sa.Integer),
                      sa.column('name', sa.String)
                      )

accommodation_type_table = sa.table('accommodation_type',
                          sa.Column('id', sa.Integer),
                          sa.Column('name', sa.String)
                          )


def upgrade():
    op.bulk_insert(amenity_table,
                   [
                       {'name': 'Wifi'},
                       {'name': 'TV'},
                       {'name': 'Kitchen'},
                       {'name': 'Pool'},
                       {'name': 'Air conditioning'},
                       {'name': 'Washer'},
                       {'name': 'Refrigerator'},
                       {'name': 'Hot water'}
                   ])
    op.bulk_insert(accommodation_type_table,
                   [
                        {'name': 'Entire Apartment'},
                        {'name': 'Entire House'},
                        {'name': 'Private Room'},
                        {'name': 'Shared Room'}
                   ])

def downgrade():
    op.execute('SET FOREIGN_KEY_CHECKS = 0')
    op.execute('TRUNCATE table amenity')
    op.execute('TRUNCATE table accommodation_type')
    op.execute('SET FOREIGN_KEY_CHECKS = 1')
