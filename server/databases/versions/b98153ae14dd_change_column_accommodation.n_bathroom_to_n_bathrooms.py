"""change column accommodation.n_bathroom to n_bathrooms

Revision ID: b98153ae14dd
Revises: 4620e1aa62f3
Create Date: 2018-12-12 16:57:17.213215

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'b98153ae14dd'
down_revision = '4620e1aa62f3'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('accommodation', 'n_bathroom', existing_type=sa.INTEGER(), new_column_name='n_bathrooms')


def downgrade():
    op.alter_column('accommodation', 'n_bathrooms', existing_type=sa.INTEGER(), new_column_name='n_bathroom')
