"""create accommodation table

Revision ID: 8de18eeb63d3
Revises: e4a7f53c3d65
Create Date: 2018-12-02 19:52:48.140571

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '8de18eeb63d3'
down_revision = 'e4a7f53c3d65'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'accommodation',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('title', sa.String(100), nullable=False),
        sa.Column('host_id', sa.Integer, sa.ForeignKey("user.id"), nullable=False, index=True),
        sa.Column('description', sa.Text, nullable=False),
        sa.Column('policy', sa.Text),
        sa.Column('district_id', sa.Integer, sa.ForeignKey("district.id"), nullable=False, index=True),
        sa.Column('latitude', sa.Float, nullable=False),
        sa.Column('longitude', sa.Float, nullable=False),
        sa.Column('pictures', sa.Text, nullable=False),
        sa.Column('type_id', sa.Integer, sa.ForeignKey("accommodation_type.id"), nullable=False, index=True),
        sa.Column('max_guests', sa.Integer, nullable=False),
        sa.Column('price', sa.Float, nullable=False),
        sa.Column('clean_fee', sa.Float, nullable=False),
        sa.Column('service_fee', sa.Float, nullable=False),
        sa.Column('n_beds', sa.Integer, nullable=False),
        sa.Column('n_bedrooms', sa.Integer, nullable=False),
        sa.Column('n_bathroom', sa.Integer, nullable=False),
    )


def downgrade():
    op.drop_table('accommodation')
