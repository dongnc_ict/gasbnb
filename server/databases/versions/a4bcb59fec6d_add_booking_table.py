"""add booking table

Revision ID: a4bcb59fec6d
Revises: 01873cf2c05b
Create Date: 2018-12-23 06:21:51.502279

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a4bcb59fec6d'
down_revision = '01873cf2c05b'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'booking',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('accommodation_id', sa.Integer, sa.ForeignKey('accommodation.id'), nullable=False, index=True),
        sa.Column('checkin', sa.Integer, nullable=False),
        sa.Column('checkout', sa.Integer, nullable=False),
        sa.Column('guests', sa.Integer, nullable=False),
        sa.Column('user_id', sa.Integer, sa.ForeignKey("user.id"), nullable=False)
    )


def downgrade():
    op.drop_table('booking')
