"""create accommodation_type, amenity tables

Revision ID: c4d36749a74e
Revises: 9e068cbaa625
Create Date: 2018-12-02 18:48:44.209955

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'c4d36749a74e'
down_revision = '9e068cbaa625'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'accommodation_type',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
    )
    op.create_table(
        'amenity',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('name', sa.String(50), nullable=False),
    )


def downgrade():
    op.drop_table('accommodation_type')
    op.drop_table('amenity')
