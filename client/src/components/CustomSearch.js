import React from 'react'
import ActionSearch from 'material-ui/svg-icons/action/search';
import { apiConst } from '../api';
import AutoComplete from 'material-ui/AutoComplete';
import CircularProgress from 'material-ui/CircularProgress';

const dataSourceConfig = {
  text: 'name',
  value: 'id',
  city: 'city'
};

class CustomSearch extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cities: undefined
    }
  }

  componentDidMount() {
    fetch(apiConst.serverURL + "/cities/")
      .then(res => res.json())
      .then(
        (result) => {
          let cities = []
          result.forEach((city) => {
            cities.push({
              id: 0,
              name: city.name,
              city: city.id
            })
            city.districts.forEach((district) => {
              cities.push({
                id: district.id,
                name: district.name + ", " + city.name,
                city: city.id
              })
            })
          });
          this.setState({
            cities: cities,
          });
        },
        (error) => {
          console.log(error)
        }
      )
  }

  render() {
    const { cities } = this.state
    return (
      <div ref={this.setWrapperRef}>
        <ActionSearch className="search-logo" />
        {!cities ?
          <CircularProgress size={60} thickness={7} /> :
          <AutoComplete
            floatingLabelText="WHERE"
            filter={AutoComplete.fuzzyFilter}
            openOnFocus={true}
            dataSource={cities}
            dataSourceConfig={dataSourceConfig}
            onNewRequest={this.props.handlePlaces}
            onKeyDown={this.props.handleInput}
            maxSearchResults={5}
          />
        }
      </div>
    );
  }
}

export default CustomSearch;