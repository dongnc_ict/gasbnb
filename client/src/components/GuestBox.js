import React from 'react'
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import GuestBoxButton from './GuestBoxButton'

class GuestBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenu: false,
      guest: {
        adults: 0,
        children: 0,
        infants: 0
      }
    }
    this.showMenu = this.showMenu.bind(this);
    this.countGuest = this.countGuest.bind(this);
    this.countAdult = this.countAdult.bind(this);
    this.countChildren = this.countChildren.bind(this);
    this.countInfant = this.countInfant.bind(this);
  }

  showMenu() {
    const { showMenu } = this.state
    this.setState({
      showMenu: !showMenu,
    }, () => {
      if (this.state.showMenu) {
        document.getElementById("guestButton").classList.add("guest-btn-focus");
      } else {
        document.getElementById("guestButton").classList.remove("guest-btn-focus");
      }
    });
  }

  countAdult(count) {
    const { adults } = this.state.guest
    this.setState({
      guest: {
        ...this.state.guest,
        adults: adults + count
      }
    }, () => {
      const { handleCount } = this.props
      handleCount(this.state.guest)
    });
  }

  countChildren(count) {
    const { adults, children } = this.state.guest
    if (adults === 0 && children === 0) {
      this.setState({
        guest: {
          ...this.state.guest,
          children: 1,
          adults: 1
        }
      }, () => {
        const { handleCount } = this.props
        handleCount(this.state.guest)
      })
    } else {
      this.setState({
        guest: {
          ...this.state.guest,
          children: children + count
        }
      }, () => {
        const { handleCount } = this.props
        handleCount(this.state.guest)
      });
    }
    const { handleCount } = this.props
    handleCount(this.state.guest)
  }

  countInfant(count) {
    const { adults, infants } = this.state.guest
    if (adults === 0 && infants === 0) {
      this.setState({
        guest: {
          ...this.state.guest,
          infants: 1,
          adults: 1
        }
      }, () => {
        const { handleCount } = this.props
        handleCount(this.state.guest)
      });
    } else {
      this.setState({
        guest: {
          ...this.state.guest,
          infants: infants + count
        }
      }, () => {
        const { handleCount } = this.props
        handleCount(this.state.guest)
      });
    }
    const { handleCount } = this.props
    handleCount(this.state.guest)
  }

  countGuest() {
    const { adults, children, infants } = this.state.guest
    if (adults === 0 && children === 0 && infants === 0) return <p>0 Guest</p>
    if (infants === 0) {
      const guests = adults + children
      return <p>{guests} guests</p>
    } else {
      const guests = adults + children
      return <p>{guests} guests, {infants} infants</p>
    }
    // if (infants === 0 && children === 0) {
    //   const guests = adults;
    //   return <p>{guests} adults</p>
    // } else if (infants !== 0 || children !== 0) {
    //   if (infants === 0) {
    //     return <p>{adults} adults, {children} children</p>
    //   } else if (children === 0) {
    //     return <p>{adults} adults, {infants} infants</p>
    //   } else {
    //     return <p>{adults} adults, {children} children, {infants} infants</p>
    //   }
    // }
  }

  render() {
    const button = this.countGuest()
    const { adults, children, infants } = this.state.guest
    return (
      <div>
        <Button id="guestButton" className="guest-btn" onClick={this.showMenu}>
          {button}
        </Button>
        {
          this.state.showMenu
            ?
            (
              <div style={{ position: 'relative' }}>
                <Paper className="popup-guest-box">
                  <GuestBoxButton title="Adult" subtitle="" count={adults} countFunc={this.countAdult} />
                  <GuestBoxButton title="Children" subtitle="Ages 2–12" count={children} countFunc={this.countChildren} />
                  <GuestBoxButton title="Infants" subtitle="Under 2" count={infants} countFunc={this.countInfant} />
                  <Button className="apply-btn" onClick={this.showMenu}>Apply</Button>
                </Paper>
              </div>
            )
            : (
              null
            )
        }
      </div>
    )
  };
}

export default GuestBox;