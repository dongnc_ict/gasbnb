import React from 'react'
import { test } from "../assets/images";
import { Carousel, Col, Row } from 'react-bootstrap';
import { Paper, Typography  } from '@material-ui/core';

const styles = {
  containerStyle: {
    width: '500px'
  },
  sliderStyle: {
    height: '150px',
    width: '150px'
  },
  logoStyle: {
    height: '150px',
    width: '150px'
  }
}

class CustomCard extends React.Component {
  render() {
    const { info } = this.props
    return (
      <Paper style={styles.containerStyle}>
        <Row>
          <Col md={4}>
            <Carousel style={styles.sliderStyle}>
              <Carousel.Item>
                <img src={test} style={styles.logoStyle} alt="test" />
              </Carousel.Item>
              <Carousel.Item>
                <img src={test} style={styles.logoStyle} alt="test" />
              </Carousel.Item>
              <Carousel.Item>
                <img src={test} style={styles.logoStyle} alt="test" />
              </Carousel.Item>
            </Carousel>
          </Col>
          <Col md={4}>
            <Typography>{info.type}</Typography>
            <Typography>{info.title}</Typography>
            <Typography>{info.room}</Typography>
            <Typography>{info.amenities}</Typography>
          </Col>
          <Col md={4}>
            <Typography>₫{info.price}</Typography>
            <Typography>per night</Typography>
            <Typography>₫{info.totalPrice} total</Typography>
            {info.superhost === true && <Typography>Superhost</Typography>}
            {info.freeCancel === true && <Typography>Free cancellation</Typography>}
          </Col>
        </Row>
      </Paper>
    );
  }
}

export default CustomCard;