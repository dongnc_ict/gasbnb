import React from 'react';
import { Col, Row } from 'react-bootstrap';

class GroupImages extends React.Component {
  render() {
    const { images } = this.props;
    return (
      <Row className="accom-group-img-container">
        <div className="accommodation-group-img">
          <Col md={6} style={{padding: '0'}}>
            <img className="accommodation-img" src={images[0]} alt="image1" />
          </Col>
          <Col md={6} style={{padding: '0'}}>
            <Row>
              <Col md={6} style={{padding: '0'}}>
                <img className="accommodation-img" src={images[1]} alt="image2" />
              </Col>
              <Col md={6} style={{padding: '0'}}>
                <img className="accommodation-img" src={images[2]} alt="image3" />
              </Col>
            </Row>
            <Row>
              <Col md={6} style={{padding: '0'}}>
                <img className="accommodation-img" src={images[3]} alt="image4" />
              </Col>
              <Col md={6} style={{padding: '0'}}>
                <img className="accommodation-img" src={images[4]} alt="image5" />
              </Col>
            </Row>
          </Col>
        </div>
      </Row>
    );
  }
}

export default GroupImages;