import React from 'react'
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

class CustomDialog extends React.Component {
  constructor(props) {
    super(props);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleOpen() {
    const { handleOpenClose } = this.props
    handleOpenClose(true)
  }

  handleClose() {
    const { handleOpenClose } = this.props
    handleOpenClose(false)
  }

  render() {
    const { children, title, onOpen } = this.props
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onClick={this.handleClose}
      />
    ];
    return (
      <div>
        <FlatButton className="header-flat-button" label={title} labelStyle={{ fontWeight: '600'}} primary={true} onClick={this.handleOpen} />
        <Dialog
          actions={actions}
          modal={false}
          open={onOpen}
          onRequestClose={this.handleClose}
        >
          {children}
        </Dialog>
      </div>
    )
  };
}

export default CustomDialog;