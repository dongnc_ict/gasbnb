import React from 'react'
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { Row, Col } from 'react-bootstrap';
import { DateUtils } from "react-day-picker";
import dateFnsFormat from "date-fns/format";
import dateFnsParse from "date-fns/parse";
import InputTitleCol from './InputTitleCol';

class CheckInOut extends React.Component {
  render() {
    const FORMAT = "dd/MM/yyyy";
    const today = new Date();
    const { checkIn, checkOut, handleCheckIn, handleCheckOut} = this.props;
    return (
      <Row>
        <Col md={6}>
          <InputTitleCol title="CHECK IN">
            <DayPickerInput
              formatDate={formatDate}
              format={FORMAT}
              parseDate={parseDate}
              placeholder="Check-in date"
              dayPickerProps={{
                selectedDays: checkIn,
                disabledDays: [
                  { before: today },
                  { after: checkOut }
                ]
              }}
              inputProps={{ readOnly: true }}
              onDayChange={handleCheckIn} />
          </InputTitleCol>
        </Col>
        <Col md={6}>
          <InputTitleCol title="CHECK OUT">
            <DayPickerInput
              formatDate={formatDate}
              format={FORMAT}
              parseDate={parseDate}
              placeholder="Check-out date"
              dayPickerProps={{
                selectedDays: checkOut,
                disabledDays: [
                  { before: today },
                  { before: checkIn }
                ]
              }}
              inputProps={{ readOnly: true }}
              onDayChange={handleCheckOut} />
          </InputTitleCol>
        </Col>
      </Row>
    );
  }
}

function parseDate(str, format, locale) {
  const parsed = dateFnsParse(str, format, { locale });
  if (DateUtils.isDate(parsed)) {
    return parsed;
  }
  return undefined;
}

function formatDate(date, format, locale) {
  return dateFnsFormat(date, format, { locale });
}

export default CheckInOut;