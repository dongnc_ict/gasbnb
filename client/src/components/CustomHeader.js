import React from 'react'
import { Col } from 'react-bootstrap';
import { withRouter } from "react-router-dom";
import CustomSearch from './CustomSearch';

class CustomHeader extends React.Component {
  constructor(props) {
    super(props)
    if (props.location.pathname === '/') {
      this.state={
        place: null,
        onFirstPage: true
      }
    }else{
      this.state={
        place: null,
        onFirstPage: false
      }
    }
    this.handlePlaces = this.handlePlaces.bind(this);
    this.onLogoClick = this.onLogoClick.bind(this)
    this.handleInput = this.handleInput.bind(this)
    this.onSearch = this.onSearch.bind(this)
  }

  componentWillMount() {
    this.unlisten = this.props.history.listen((location, action) => {
      if(location.pathname === '/'){
        this.setState({onFirstPage:true})
      }else{
        this.setState({onFirstPage:false})
      }
    });
  }

  componentWillUnmount() {
      this.unlisten();
  }

  onLogoClick() {
    this.props.history.push('/');
  }

  onSearch() {
    const { place } = this.state
    if (!place) {
      this.props.history.push('/search/cities/1');
    } else if (place.id === 0) {
      this.props.history.push('/search/cities/' + place.city);
    } else {
      this.props.history.push('/search/cities/' + place.city + "/districts/" + place.id);
    }
  }

  handlePlaces(chosenRequest) {
    this.setState({ place: chosenRequest});
  }

  handleInput(e){
    if(e.keyCode == 13){
      this.onSearch()
    }
 }

  render() {
    const { logo, children } = this.props
    const {onFirstPage} = this.state
    if (onFirstPage) {
      return (
        <Col className="landing-header" md={12} sm={12} lg={12}>
          <Col className="no-padding" md={2} sm={2} lg={2}>
            <img className="header-logo" src={logo} alt="logo" onClick={this.onLogoClick} />
          </Col>
          <Col md={10} sm={10} lg={10} className="header-buttons">
            {children}
          </Col>
        </Col>
      )
    } else {
      return (
        <Col className="other-header" md={12} sm={12} lg={12}>
          <Col className="no-padding" md={1} sm={1} lg={1}>
            <img className="header-logo" src={logo} alt="logo" onClick={this.onLogoClick} />
          </Col>
          <Col md={3} sm={3} lg={3} className="margin-top-10">
              <CustomSearch handlePlaces={this.handlePlaces} handleInput={this.handleInput}/>
          </Col>
          <Col md={8} sm={8} lg={8} className="header-buttons">
            {children}
          </Col>
        </Col>
      )
    }
  };
}

export default withRouter(CustomHeader);