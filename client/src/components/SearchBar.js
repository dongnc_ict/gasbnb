import React from 'react'
import TextField from 'material-ui/TextField';
import SearchForm from './SearchForm'
import ActionSearch from 'material-ui/svg-icons/action/search';
import '../css/style.css'

const styles = {
  containerStyle:{

  },
  logoStyle:{

  },
  textFieldStyle:{

  }
}

class SearchBar extends React.Component {
  constructor(props) {
    super(props)
    if (props.location.includes("search")){
      this.state = {
        searchText: props.location.replace("/search/", ""),
        onSearch: false
      }
    }else{
      this.state = {
        searchText: "",
        onSearch: false
      }
    }
    this.onTextChange = this.onTextChange.bind(this)
    this.onClick = this.onClick.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    this.setWrapperRef = this.setWrapperRef.bind(this);
  }

  onTextChange(event, newValue) {
    this.setState({
      searchText: newValue
    })
  }

  onClick() {
    this.setState({
      onSearch: true
    })
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickOutside(event){
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        onSearch: false
      })
    }
  }

  setWrapperRef(node) {
    this.wrapperRef = node;
  }

  render() {
    const { onSearch } = this.state
    const { location } = this.props
    return (
      <div ref={this.setWrapperRef} className="search-bar">
        <ActionSearch className="search-logo"/>
        <TextField
          value={this.state.searchText}
          onChange={this.onTextChange}
          onClick={this.onClick}
          style={{width: '90%'}}
        />
        {onSearch && location.includes("search") &&
          <SearchForm onSearchBar={true} place={location}/>}
      </div>
    );
  }
}

export default SearchBar;