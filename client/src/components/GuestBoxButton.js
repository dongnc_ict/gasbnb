import React from 'react'
import { Col, Row } from 'react-bootstrap';
import ContentAddCircleOutline from 'material-ui/svg-icons/content/add-circle-outline';
import ContentRemoveCircleOutline from 'material-ui/svg-icons/content/remove-circle-outline';

const styles = {
  boxStyle: {
    height: 60
  }
}

class GuestBoxButton extends React.Component {
  constructor(props) {
    super(props);
    this.add = this.add.bind(this);
    this.remove = this.remove.bind(this);
  }

  add() {
    const { countFunc } = this.props
    countFunc(1)
  }

  remove() {
    const { countFunc } = this.props
    countFunc(-1)
  }

  render() {
    const { title, subtitle } = this.props
    return (
      <Row style={styles.boxStyle}>
        <Col md={6}>
          <p className="guest-type-title">{title}</p>
          <p className="guest-type-subtitle"> {subtitle}</p>
        </Col>
        <Col md={6}>
          {this.props.count > 0 ?
            <Col md={4} onClick={this.remove}>
              <ContentRemoveCircleOutline />
            </Col> :
            <Col md={4}>
              <ContentRemoveCircleOutline />
            </Col>
          }
          <Col md={4}>
            {this.props.count}
          </Col>
          <Col md={4} onClick={this.add}>
            <ContentAddCircleOutline />
          </Col>
        </Col>
      </Row>
    )
  };
}

export default GuestBoxButton;