import React from 'react'
import Typography from '@material-ui/core/Typography';

const styles = {
  titleStyle: {

  },
  titleBoxStyle: {

  },
  mainBoxStyle: {

  },
  containerStyle: {

  }
}

class InputTitleCol extends React.Component {
  render() {
    const { title, children } = this.props
    return (
      <div style={styles.containerStyle}>
        <div style={styles.titleBoxStyle}>
          <Typography variant="h5" gutterBottom style={styles.titleStyle}>
            {title}
          </Typography>
        </div>
        <div style={styles.mainBoxStyle}>
          {children}
        </div>
      </div>
    );
  }
}

export default InputTitleCol;