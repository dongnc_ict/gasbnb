import React from 'react';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import InputTitleCol from './InputTitleCol';
import GuestBox from './GuestBox';
import { withRouter } from "react-router-dom";
import CheckInOut from './CheckInOut'
import 'react-day-picker/lib/style.css';
import CustomSearch from './CustomSearch';

const styles = {
  divStyle: {
    width: 400,
    margin: 20,
    padding: 20
  }
}

class SearchForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      place: null,
      checkIn: undefined,
      checkOut: undefined,
      guest: {
        adults: 0,
        children: 0,
        infants: 0
      }
    };
    this.handlePlaces = this.handlePlaces.bind(this);
    this.handleCheckIn = this.handleCheckIn.bind(this);
    this.handleCheckOut = this.handleCheckOut.bind(this);
    this.handleCount = this.handleCount.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.searchButton = this.searchButton.bind(this);
    this.searchField = this.searchField.bind(this);
    this.handleInput = this.handleInput.bind(this);
  }

  handlePlaces(chosenRequest) {
    this.setState({ place: chosenRequest });
  }

  handleCheckIn(day) {
    this.setState({ checkIn: day });
  }

  handleCheckOut(day) {
    this.setState({ checkOut: day });
  }

  handleCount(guest) {
    this.setState({ guest: guest });
  }

  onSearch() {
    const { place } = this.state
    if (!place) {
      this.props.history.push('/search/cities/1');
    } else if (place.id === 0) {
      this.props.history.push('/search/cities/' + place.city);
    } else {
      this.props.history.push('/search/cities/' + place.city + "/districts/" + place.id);
    }
  }

  handleInput(e){
    if(e.keyCode == 13){
      this.onSearch()
    }
 }

  searchButton(onSearchBar) {
    if (onSearchBar === false) {
      return (
        <Button className="search-btn margin-top-10" onClick={this.onSearch}>
          <Typography>Search</Typography>
        </Button>
      )
    }
  }

  searchField(onSearchBar, url) {
    if (onSearchBar === true) {
      console.log(url)
      return (
        <Typography variant="h2" gutterBottom>
          Explore
        </Typography>
      )
    } else {
      return (
        <div>
          <Typography variant="h2" gutterBottom>
            Book unique homes and experiences.
          </Typography>
          <CustomSearch handlePlaces={this.handlePlaces} handleInput={this.handleInput}/>
        </div>
      )
    }
  }

  render() {
    const { onSearchBar, url } = this.props;
    const { checkIn, checkOut } = this.state
    return (
      <Paper style={styles.divStyle}>
        {this.searchField(onSearchBar, url)}
        <CheckInOut checkIn={checkIn} checkOut={checkOut} handleCheckIn={this.handleCheckIn} handleCheckOut={this.handleCheckOut} />
        <InputTitleCol title="GUESTS">
          <GuestBox guest={this.state.guest} handleCount={this.handleCount} />
        </InputTitleCol>
        {this.searchButton(onSearchBar)}
      </Paper>
    )
  }
}

export default withRouter(SearchForm);
