import React from 'react'
class CustomButton extends React.Component {
    render() {
        const {name,onClick,style} = this.props;
        return (
            <button className="square" onClick={onClick} style={style}>
                {name}
            </button>
        );
    }
}

export default CustomButton;