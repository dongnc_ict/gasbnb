export function search(place) {
	return {
		type: "SEARCH",
		place: place
	}
}