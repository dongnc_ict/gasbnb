import { userConstants } from '../constants';

function login(user) {
    return {
		type: userConstants.LOGIN_REQUEST,
		user
	};
}

function logout() {
    return { type: userConstants.LOGOUT };
}

export const userActions = {
    login,
    logout
};