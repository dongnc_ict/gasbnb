const apiConst = {
  serverURL:"http://127.0.0.1:5000",
  hanoiCity:"/cities/1/accommodations?limit=4&offset=0",
  hcmCity:"/cities/2/accommodations?limit=4&offset=0"
};

const getCityApi = (cityId,limit,offset) =>{
  return "http://127.0.0.1:5000/cities/" + cityId + "/accommodations?limit=" + limit + "&offset=" + offset
}

const getDistrictApi = (districtId,limit,offset) =>{
  return "http://127.0.0.1:5000/districts/" + districtId + "/accommodations?limit=" + limit + "&offset=" + offset
}

const getAccomApi = (id) =>{
  return "http://127.0.0.1:5000/accommodations/" + id
}

// function get(url) {
//   const myInit = {
//     method: 'GET',
//     mode: 'no-cors',
//     headers: {
//       'Content-Type': 'application/json;charset=UTF-8',
//       'Access-Control-Allow-Origin': '*'
//     }
//   };
//   const myRequest = new Request(url, myInit);

//   fetch(apiConst.serverURL + url)
//     .then(res => res.json())
//     .then(
//       (result) => {
//         console.log(result)
//         return result
//       },
//       (error) => {
//         return error
//       }
//     )
// }

export {apiConst, getCityApi, getDistrictApi, getAccomApi};