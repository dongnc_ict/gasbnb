import { userConstants } from '../constants';
const initState = {
  user: null
}

function userReducer (state = initState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      console.log("action",action)
      return {
        ...state,
        user: action.user
      }
    case userConstants.LOGOUT:
    return {
      ...state,
      user: null
    }
    default:
      return state
  }
}
export default userReducer