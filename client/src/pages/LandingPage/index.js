import React from 'react';
import withRoot from '../../withRoot';
import { test } from "../../assets/images";
import { Col, Row } from 'react-bootstrap';
import { Paper } from '@material-ui/core';
import { SearchForm } from '../../components';
import { apiConst, getCityApi } from '../../api';
import CitySlider from './CitySlider';
import CircularProgress from 'material-ui/CircularProgress';

class LandingPage extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      types: null,
      hanoiCity: null,
      hcmCity: null
    }
    this.renderTypes = this.renderTypes.bind(this)
  }

  componentDidMount() {
    fetch(apiConst.serverURL + "/types/")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            types: result,
          });
        },
        (error) => {
          console.log(error)
        }
      )
    fetch(getCityApi(1,4,0))
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            hanoiCity: result
          });
        },
        (error) => {
          console.log(error)
        }
      )
    fetch(getCityApi(2,4,0))
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            hcmCity: result
          });
        },
        (error) => {
          console.log(error)
        }
      )
  }

  onTypeClick(type) {
    console.log(type)
  }

  renderTypes(types) {
    return (
      types.map((type) =>
        <Paper className="category-box" onClick={() => this.onTypeClick(type)} key={type.id}>
          <div>
            <img className="category-img" src={test} alt="test" />
          </div>
          <div className="category-name">
            <h5>{type.name}</h5>
          </div>
        </Paper>
      )
    )
  }

  render() {
    const { types, hanoiCity, hcmCity } = this.state;
    let typeBar = null;
    if (types) {
      typeBar = this.renderTypes(types)
    }

    return (
      <Col md={12} sm={12} lg={12} className="no-padding">
        <div className="root">
          <Row className="search-row">
            <SearchForm onSearchBar={false} />
          </Row>
        </div>
        <div style={{ textAlign: 'center' }}>
          <Row className="margin-top-30 margin-bottom-50">
            {!typeBar ?
              <CircularProgress size={60} thickness={7} /> :
              <div style={{ display: 'inline-block' }}>
                <h2 className="explore-title">Explore Gasbnb</h2>
                {typeBar}
              </div>
            }
          </Row>
          <Row className="margin-top-30 margin-bottom-50">
            {!hanoiCity ?
              <CircularProgress size={60} thickness={7} /> :
              <CitySlider name={"Ha Noi City"} city={hanoiCity} />
            }
          </Row>
          <Row className="margin-top-30">
            {!hcmCity ?
              <CircularProgress size={60} thickness={7} /> :
              <CitySlider name={"Ho Chi Minh City"} city={hcmCity} />
            }
          </Row>
          <div style={{height: '100px'}}></div>
        </div>
      </Col>
    );
  }
}

export default withRoot(LandingPage);