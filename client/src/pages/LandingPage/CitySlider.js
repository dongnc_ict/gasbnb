import React from 'react';
import { Paper } from '@material-ui/core';
import { withRouter } from "react-router-dom";

class CitySlider extends React.Component {
	constructor(props) {
		super(props)
    this.renderCity = this.renderCity.bind(this)
    this.onHouseClick = this.onHouseClick.bind(this)
  }

  onHouseClick(id) {
    this.props.history.push('/detail/' + id);
  }

	renderCity() {
		const { city } = this.props
		if (city) {
			return (
				city.accommodations.map((accommodation) =>
					<div className="accommodation-container" key={accommodation.id}>
						<Paper style={{ height: '100%' }} onClick={()=>this.onHouseClick(accommodation.id)}>
							<div>
								<img src={accommodation.pictures[0]} className="accommodation-thumb" alt="accommodation"/>
							</div>
							<p className="accommodation-type">{accommodation.type}</p>
							<h4 className="accommodation-title">{accommodation.title}</h4>
						</Paper>
					</div>
				)
			)
		}
	}

	render() {
		const { name } = this.props;
		const citySlider = this.renderCity();
		return (
			<div style={{ display: 'inline-block' }}>
				<h2 className="explore-title">Explore {name}</h2>
				<div className="city-slider">
					{citySlider}
				</div>
			</div>
		)
	}
}

export default withRouter(CitySlider);