import React from 'react';
import { withRouter } from "react-router-dom";
import SearchResult from './SearchResult';
import Pagination from "react-js-pagination";
import { getCityApi,getDistrictApi } from '../../api';
import CircularProgress from 'material-ui/CircularProgress';

const styles = {
  containerStyle: {

  },
  cardStyle: {

  }
}
class SearchPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activePage: undefined,
      accommodations: undefined
    };
    this.handlePageChange = this.handlePageChange.bind(this)
    this.onHouseClick = this.onHouseClick.bind(this)
  }

  componentDidMount() {
    const {location} = this.props;
    var res = location.pathname.split("/");
    if(res.length === 6){
      console.log(getDistrictApi(res[5],10,0))
      fetch(getDistrictApi(res[5],10,0))
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            accommodations: result.accommodations
          });
        },
        (error) => {
          console.log(error)
        }
      )
    }else{
      console.log(getCityApi(res[3],10,0))
      fetch(getCityApi(res[3],10,0))
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            accommodations: result.accommodations
          });
        },
        (error) => {
          console.log(error)
        }
      )
    }
  }

  handlePageChange(pageNumber) {
    console.log(`active page is ${pageNumber}`);
    this.setState({ activePage: pageNumber });
  }

  onHouseClick(id) {
    this.props.history.push('/detail/' + id);
  }

  render() {
    const { accommodations } = this.state;
    return (
      <div>
        <div style={styles.containerStyle}>
          {!accommodations ?
            <CircularProgress size={60} thickness={7} /> :
            <SearchResult accommodations = {accommodations} onHouseClick={this.onHouseClick}/>
          }
        </div>
        <div>
          <Pagination
            activePage={this.state.activePage}
            itemsCountPerPage={10}
            totalItemsCount={450}
            pageRangeDisplayed={5}
            onChange={this.handlePageChange}
          />
        </div>
      </div>
    );
  }
}

export default withRouter(SearchPage);