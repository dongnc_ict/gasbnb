import React from 'react';
import { Paper } from '@material-ui/core';
import { Col, Row, Carousel } from 'react-bootstrap';
const styles = {
  containerStyle: {
    width: '500px'
  },
  sliderStyle: {
    height: '150px',
    width: '150px'
  },
  logoStyle: {
    height: '150px',
    width: '150px'
  }
}

class SearchResult extends React.Component {
  constructor(props) {
    super(props)
    this.renderResult = this.renderResult.bind(this)
  }

  renderResult() {
    const { accommodations, onHouseClick } = this.props
    if (accommodations) {
      return (
        accommodations.map((accommodation) =>
          <Paper key={accommodation.id}>
            <Row>
              <Col md={4}>
                <Carousel style={styles.sliderStyle}>
                  <Carousel.Item>
                    <img src={accommodation.pictures[0]} style={styles.logoStyle} alt="accommodation 0"/>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img src={accommodation.pictures[1]} style={styles.logoStyle} alt="accommodation 1"/>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img src={accommodation.pictures[2]} style={styles.logoStyle} alt="accommodation 2"/>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img src={accommodation.pictures[3]} style={styles.logoStyle} alt="accommodation 3"/>
                  </Carousel.Item>
                  <Carousel.Item>
                    <img src={accommodation.pictures[4]} style={styles.logoStyle} alt="accommodation 4"/>
                  </Carousel.Item>
                </Carousel>
              </Col>
              <Col md={4} onClick={()=>onHouseClick(accommodation.id)}>
                  <p>{accommodation.type}</p>
                  <p>{accommodation.title}</p>
                  <p>{accommodation.nBeds} beds</p>
                </Col>
                <Col md={4}>
                  <p>${accommodation.price}</p>
                  <p>per night</p>
                  <p>${accommodation.totalPrice} total</p>
                </Col>
            </Row>
          </Paper>
        )
      )
    }
  }

  render() {
    const results = this.renderResult();
    return (
      <div>
        {results}
      </div>
    )
  }
}

export default SearchResult;