import React from 'react';
import withRoot from '../../withRoot';
import { Col, Row } from 'react-bootstrap';
import { GroupImages } from '../../components';
import AcomDescript from './AcomDescript';
import BookBox from './BookBox';
import { getAccomApi } from '../../api';
import CircularProgress from 'material-ui/CircularProgress';

class DetailPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      info: null
    }
  }

  componentDidMount() {
    const id = this.props.match.params.id
    fetch(getAccomApi(id))
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result)
          this.setState({
            info: result
          });
        },
        (error) => {
          console.log(error)
        }
      )
  }

  render() {
    const { info } = this.state;
    return (
      <div>
        {
          !info ?
            <CircularProgress size={60} thickness={7} /> :
            <div>
              <GroupImages images={info.pictures} />
              <Row>
                <Col md={8}>
                  <AcomDescript info={info}/>
                </Col>
                <Col md={4}>
                  <BookBox info={info}/>
                </Col>
              </Row>
            </div>
        }
      </div>
    );
  }
}

export default withRoot(DetailPage);