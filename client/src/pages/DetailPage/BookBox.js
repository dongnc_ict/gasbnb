import React from 'react'
import Paper from '@material-ui/core/Paper';
import FlatButton from 'material-ui/FlatButton';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import { Row, Col } from 'react-bootstrap';
import { GuestBox, InputTitleCol } from '../../components'
class BookBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkIn: undefined,
      checkOut: undefined,
      guest: {
        adults: 0,
        children: 0,
        infants: 0
      }
    };
    this.handleCheckIn = this.handleCheckIn.bind(this);
    this.handleCheckOut = this.handleCheckOut.bind(this);
    this.handleCount = this.handleCount.bind(this);
  }

  handleCheckIn(day) {
    this.setState({ checkIn: day });
  }

  handleCheckOut(day) {
    this.setState({ checkOut: day });
  }

  handleCount(guest) {
    this.setState({ guest: guest });
  }

  render() {
    const {info} = this.props
    const night = 9
    return (
      <Paper>
        <div>${info.price} per night</div>
        <Row>
          <Col md={6}>
            <InputTitleCol title="CHECK IN">
              <DayPickerInput onDayChange={this.handleCheckIn} />
            </InputTitleCol>
          </Col>
          <Col md={6}>
            <InputTitleCol title="CHECK OUT">
              <DayPickerInput onDayChange={this.handleCheckOut} />
            </InputTitleCol>
          </Col>
        </Row>
        <InputTitleCol title="GUESTS">
          <GuestBox guest={this.state.guest} handleCount={this.handleCount} />
        </InputTitleCol>
        <Row>
          <Col md={6}>
            <div>${info.price} x {night} nights</div>
          </Col>
          <Col md={6}>
            <div>${info.price*night}</div>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <div>Service fee</div>
          </Col>
          <Col md={6}>
            <div>${info.serviceFee}</div>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <div>Cleaning fee</div>
          </Col>
          <Col md={6}>
            <div>${info.cleanFee}</div>
          </Col>
        </Row>
        <Row>
          <Col md={6}>
            <div>Total</div>
          </Col>
          <Col md={6}>
            <div>${info.price*night + info.serviceFee + info.cleanFee}</div>
          </Col>
        </Row>
        <FlatButton label="Book" onClick={()=>console.log("Book")}/>
        <div>You won’t be charged yet</div>
      </Paper>
    );
  }
}

export default BookBox;