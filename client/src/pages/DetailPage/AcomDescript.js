import React from 'react'
import { Col, Row } from 'react-bootstrap';
import Avatar from 'material-ui/Avatar';
import Paper from '@material-ui/core/Paper';
import GoogleMapReact from 'google-map-react';
import { test } from '../../assets/images';
import { MapComponent } from '../../components';
import { connect } from 'react-redux';

class AcomDescript extends React.Component {
  render() {
    const {info,user} = this.props
    console.log(user)
    const amenities = info.amenities.map((amenity)=>
      <div key={amenity.id}>
        {amenity.name}
      </div>
    )
    const center = {
      lat: info.latitude,
      lng: info.longitude
    }
    return (
      <div>
        <Row>
          <Col md={10}>
            <div>{info.type}</div>
            <div>{info.title}</div>
            <div>{info.district}, {info.city}</div>
          </Col>
          <Col md={2}>
            <Avatar src={test} />
            <div>Name</div>
          </Col>
        </Row>
        <Row>
          <div>{info.maxGuests} guests {info.nBedrooms} bedrooms {info.nBathrooms} bathrooms</div>
        </Row>
        <Row>
          <Paper>
            <div dangerouslySetInnerHTML={{__html:info.description}}/>
          </Paper>
          <div>Amenities</div>
          <div>{amenities}</div>
        </Row>
        <Row>
          <div style={{ height: '100vh', width: '100%' }}>
            <GoogleMapReact
              defaultCenter={center}
              defaultZoom={15}
            >
              <MapComponent
                lat={info.latitude}
                lng={info.longitude}
              />
            </GoogleMapReact>
          </div>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      user: state.user
  };
};

export default connect(mapStateToProps)(AcomDescript);