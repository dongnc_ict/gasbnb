import React from 'react';
import { logoIma } from "../../assets/images";
import { CustomDialog, CustomHeader } from '../../components';
import FlatButton from 'material-ui/FlatButton';
import { provider, auth } from "../../base";
import SignUp from './SignUp';
import LogIn from './LogIn';
import { apiConst } from '../../api';
import {userActions} from '../../actions/user.action'
import { connect } from 'react-redux';

class Header extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			onSignUp: false,
			onLogIn: false,
			onHelp: false,
      onBecomeHost: false,
      user:null
		}
		this.handleOnSignUp = this.handleOnSignUp.bind(this)
		this.handleOnLogIn = this.handleOnLogIn.bind(this)
		this.handleOnHelp = this.handleOnHelp.bind(this)
		this.handleBecomeHost = this.handleBecomeHost.bind(this)
		this.facebookLogin = this.facebookLogin.bind(this);
		this.logout = this.logout.bind(this);
	}

	facebookLogin() {
    const { dispatch } = this.props;
		auth().signInWithPopup(provider)
			.then(({ user, credential }) => {
        fetch(apiConst.serverURL+"/login/fb", {
          method: 'post',
          headers:{
            'Content-Type': 'application/json;charset=UTF-8'
          },
          body: JSON.stringify({
           "email": user.email,
           "name": user.displayName,
           "accessToken":credential.accessToken
          })
         }).then(res => res.json())
         .then(
           (result) => {
            dispatch(userActions.login(result))
            this.setState({onSignUp:false,onLogIn:false,user:result})
           },
           (error) => {
             console.log(error)
           }
         );
			})
	}

	logout() {
    const { dispatch } = this.props;
		auth().signOut().then(() => {
      dispatch(userActions.logout())
      this.setState({user:null})
		})
	}

	handleOnSignUp(onOpen) {
		this.setState({
			onSignUp: onOpen
		})
	}

	handleOnLogIn(onOpen) {
		this.setState({
			onLogIn: onOpen
		})
	}

	handleOnHelp(onOpen) {
		this.setState({
			onHelp: onOpen
		})
	}

	handleBecomeHost(onOpen) {
		this.setState({
			onBecomeHost: onOpen
		})
	}

	render() {
    const { onHelp, onBecomeHost, onSignUp, onLogIn, user } = this.state
		return (
			<div>
				<CustomHeader logo={logoIma}>
					<div className="header-flat-button">
						<CustomDialog title="Become a host" onOpen={onBecomeHost} handleOpenClose={this.handleBecomeHost}>
							<p>Become a host</p>
						</CustomDialog>
					</div>
					<div className="header-flat-button">
						<CustomDialog title="Help" onOpen={onHelp} handleOpenClose={this.handleOnHelp}>
							<p>Help</p>
						</CustomDialog>
					</div>
					<div className="header-flat-button">
						{!user &&
							<SignUp
								handleOnSignUp={this.handleOnSignUp}
								handleOnLogIn={this.handleOnLogIn}
								onSignUp={onSignUp}
								facebookSignUp={this.facebookLogin}
							/>
						}
					</div>
					<div className="header-flat-button">
						{user ?
							<div>
								<FlatButton
									className="header-flat-button"
									label={"Log out"}
									primary={true}
									onClick={this.logout}
								/>
								<p>{user.firstName}</p>
							</div>
							:
							<LogIn
								handleOnLogIn={this.handleOnLogIn}
								handleOnSignUp={this.handleOnSignUp}
								onLogIn={onLogIn}
								facebookLogin={this.facebookLogin}
							/>
						}
					</div>
				</CustomHeader>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
  return {
      user: state.user
  };
};
export default connect(mapStateToProps)(Header);