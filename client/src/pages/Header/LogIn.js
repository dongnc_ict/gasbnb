import React from 'react'
import { Col, Row } from 'react-bootstrap';
import { CustomDialog } from '../../components'
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import { apiConst } from '../../api';

const styles = {
  checkbox: {
    marginBottom: 16,
  },
}

class LogIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: ""
    }
    this.signUp = this.signUp.bind(this)
    this.onEmailChange = this.onEmailChange.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
    this.logIn = this.logIn.bind(this)
  }

  signUp(){
    const { handleOnLogIn,handleOnSignUp } = this.props
    handleOnLogIn(false)
    handleOnSignUp(true)
  }

  onEmailChange(event, newValue) {
    this.setState({
      email: newValue
    })
  }

  onPasswordChange(event, newValue) {
    this.setState({
      password: newValue
    })
  }

  logIn(){
    const {email,password}=this.state
    fetch(apiConst.serverURL+"/login", {
      method: 'post',
      headers:{
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
       "email": email,
       "password": password
      })
     }).then(res => res.json())
     .then(
       (result) => {
          console.log(result);
       },
       (error) => {
         console.log(error)
       }
     );
  }

  render() {
    const { onLogIn, handleOnLogIn, facebookLogin} = this.props
    const { email, password} = this.state
    return (
      <CustomDialog title="Log in" onOpen={onLogIn} handleOpenClose={handleOnLogIn}>
        <Row>
          <FlatButton
            label="Log in with Facebook"
            onClick={() => facebookLogin()}
          />
        </Row>
        <Row>
          <FlatButton
            label="Log in with Google"
          />
        </Row>
        <Divider />
        <Row>
          <TextField
            floatingLabelText="Email Address"
            value={email}
            onChange={this.onEmailChange}
          />
        </Row>
        <Row>
          <TextField
            floatingLabelText="Password"
            value={password}
            onChange={this.onPasswordChange}
          />
        </Row>
        <Row>
          <FlatButton
            label="Log in"
            onClick={this.logIn}
          />
        </Row>
        <Row>
          <Col md={8}>
            <Checkbox
              label="Remember me"
              style={styles.checkbox}
            />
          </Col>
          <Col md={4}>
            <FlatButton label="Show Password" />
          </Col>
        </Row>
        <Divider />
        <Row>
          <Col md={6}>
            <p>Don’t have an account?</p>
          </Col>
          <Col md={6}>
            <FlatButton
              label="Sign up"
              onClick={this.signUp}
            />
          </Col>
        </Row>
      </CustomDialog>
    )
  }
}

export default LogIn;