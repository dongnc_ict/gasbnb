import React from 'react'
import { Col, Row } from 'react-bootstrap';
import { CustomDialog } from '../../components'
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import { apiConst } from '../../api';

class SignUp extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: ""
    }
    this.logIn = this.logIn.bind(this)
    this.signUp = this.signUp.bind(this)
    this.onFirstNameChange = this.onFirstNameChange.bind(this)
    this.onLastNameChange = this.onLastNameChange.bind(this)
    this.onEmailChange = this.onEmailChange.bind(this)
    this.onPasswordChange = this.onPasswordChange.bind(this)
  }

  logIn() {
    const { handleOnSignUp, handleOnLogIn } = this.props;
    handleOnSignUp(false);
    handleOnLogIn(true);
  }

  onFirstNameChange(event, newValue) {
    this.setState({
      firstName: newValue
    })
  }

  onLastNameChange(event, newValue) {
    this.setState({
      lastName: newValue
    })
  }

  onEmailChange(event, newValue) {
    this.setState({
      email: newValue
    })
  }

  onPasswordChange(event, newValue) {
    this.setState({
      password: newValue
    })
  }

  signUp(){
    const {firstName,lastName,email,password}=this.state
    fetch(apiConst.serverURL+"/signup", {
      method: 'post',
      headers:{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: JSON.stringify({
       "email": email,
       "password": password,
       "firstName": firstName,
       "lastName": lastName,
      })
     }).then(res => res.json())
     .then(
       (result) => {
          console.log(result);
       },
       (error) => {
         console.log(error)
       }
     );
  }

  render() {
    const { onSignUp, handleOnSignUp , facebookSignUp} = this.props;
    const {firstName, lastName, email, password} = this.state
    return (
      <CustomDialog title="Sign up" onOpen={onSignUp} handleOpenClose={handleOnSignUp}>
        <Row>
          <FlatButton
            label="Continue with Facebook"
            onClick={() => facebookSignUp()}
          />
        </Row>
        <Row>
          <FlatButton
            label="Continue with Google"
          />
        </Row>
        <Divider />
        <Row>
          <TextField
            floatingLabelText="First Name"
            value={firstName}
            onChange={this.onFirstNameChange}
          />
        </Row>
        <Row>
          <TextField
            floatingLabelText="Last Name"
            value={lastName}
            onChange={this.onLastNameChange}
          />
        </Row>
        <Row>
          <TextField
            floatingLabelText="Email Address"
            value={email}
            onChange={this.onEmailChange}
          />
        </Row>
        <Row>
          <TextField
            floatingLabelText="Password"
            value={password}
            onChange={this.onPasswordChange}
          />
        </Row>
        <FlatButton
            label="Sign Up"
            onClick={() => this.signUp()}
          />
        <Divider />
        <Col md={6}>
          <p>Already have an Airbnb account?</p>
        </Col>
        <Col md={6}>
          <FlatButton
            label="Log in"
            onClick={this.logIn}
          />
        </Col>
      </CustomDialog>

    )
  }
}

export default SignUp;