import React from "react";
import { BrowserRouter as Router, Route } from 'react-router-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import LandingPage from './LandingPage';
import SearchPage from './SearchPage';
import DetailPage from './DetailPage/DetailPage';
import Header from "./Header";
import '../css/style.css'

function Root() {
  return (
    <Router>
      <MuiThemeProvider>
        <div>
          <Header/>
          <div>
            <Route exact path="/" component={LandingPage} />
            <Route path='/search/cities/:city_id' component={SearchPage} />
            <Route path='/search/cities/:city_id/districts/:district_id' component={SearchPage} />
            <Route path='/detail/:id' component={DetailPage} />
          </div>
        </div>
      </MuiThemeProvider>
    </Router>
  )
}
export default Root;