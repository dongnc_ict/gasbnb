import React from 'react';
import ReactDOM from 'react-dom';
import Root from './pages';
import rootReducer from './reducers';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import * as serviceWorker from './serviceWorker';

const store = createStore(rootReducer)
ReactDOM.render(
  <Provider store={store}>
      <Root />
  </Provider>,
  document.getElementById('root')
);

serviceWorker.unregister();