# SEA Group 4 - Airbnb clone
___
## Installation instruction
1. From project root, run below commands to install all dependent packages
    ```
    cd server
    pip install -r requirements.txt
    ```
2. Duplicate **alembic.ini.example** file and rename it to **alembic.ini**. Then change:
    ```
    sqlalchemy.url = driver://user:pass@localhost/dbname
    ```

    to your database drive, name, username and password.  
    For example, because we use MySQL, and database name is "_gasbnb_":
    ```
    sqlalchemy.url = mysql://user:pass@localhost/gasbnb
    ```
3. Import database by running:
    ```
    alembic upgrade head
    ```
4. In **app** directory, duplicate **instance.py.example** file and rename it to **instance.ini**. Then change:
    ```
    ENV = 'test'
    ```
    to environment: *dev*, *prod*, *test*
    ```
    DATABASE_CONFIG = {
        'host': 'localhost',
        'dbname': 'gasbnb',
        'user': 'root',
        'password': '',
        'port': 3306
    }
    ```
    to your database drive, name, username and password. 
    
5. Run:
    ```
    python manage.py run